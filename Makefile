all:
	gcc -g -c -Wall scheduler.c -o scheduler.o
	gcc -g -Wall scheduler.o -o scheduler

clean:
	rm *.o
	rm scheduler
